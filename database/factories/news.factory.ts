import { hash } from 'bcrypt';
import { setSeederFactory } from 'typeorm-extension';

import { News,NewsLocation,NewsAuthor } from 'src/news/entities';

export default setSeederFactory(News, async (faker) => {
  const news = new News();
  const location = new NewsLocation();
  const author = new NewsAuthor();
  const optionsDate = {
    refDate: new Date('2022-01-15T01:33:29.567Z').toISOString(), // Fecha de referencia para el rango
  };

  news.titulo_noticia = faker.lorem.sentences(6);
  news.contenido = faker.lorem.sentences(150);
  const fechaRandom = faker.date.anytime(optionsDate);
  const fechaString: string = fechaRandom.toISOString();
  news.fecha = fechaString;
  location.nombre_lugar = faker.location.city();
  author.nombre_autor = faker.person.fullName();

  return news;
}); 