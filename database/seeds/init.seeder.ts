import { DataSource } from 'typeorm';
import { runSeeders, Seeder, SeederFactoryManager } from 'typeorm-extension';

import newsFactory from 'database/factories/news.factory';
//import userFactory from 'database/factories/user.factory';
import authorSeeder from './author.seeder';
import locationSeeder from './location.seeder';
import newsSeeder from './news.seeder';

export default class InitSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {
    await runSeeders(dataSource, {
      seeds: [authorSeeder, locationSeeder, newsSeeder],
      factories: [newsFactory],
    });
  }
}