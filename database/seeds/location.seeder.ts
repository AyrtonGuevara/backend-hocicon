import { hash } from 'bcrypt';
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { News,NewsLocation,NewsAuthor } from 'src/news/entities';

export default class locationSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {
    const repository = dataSource.getRepository(NewsLocation);

    const data = {
      nombre_lugar: 'La Paz',
    };

    const news = await repository.findOneBy({ nombre_lugar: data.nombre_lugar });

    // Insert only one record with this username.
    if (!news) {
      await repository.insert([data]);
    }

    // ---------------------------------------------------

    const userFactory = await factoryManager.get(NewsLocation);


    // Insert many records in database.
    await userFactory.saveMany(5)
  }
}