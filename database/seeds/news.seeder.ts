import { hash } from 'bcrypt';
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { News,NewsLocation,NewsAuthor } from 'src/news/entities';

export default class newsSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {
    const repository = dataSource.getRepository(News);
    const repositoryAuthors = dataSource.getRepository(NewsAuthor);
    const repositoryLocations = dataSource.getRepository(NewsLocation);

    const data = {
      titulo_noticia: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
      contenido: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
      fecha: '21-11-2021',
      lugar: new NewsLocation().nombre_lugar,
      autor: new NewsAuthor().nombre_autor,
    };

    // ---------------------------------------------------

    const newsFactory = await factoryManager.get(News);


    // Insert many records in database.
    await newsFactory.saveMany(10)
  }
}