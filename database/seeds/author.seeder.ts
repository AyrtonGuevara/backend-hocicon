import { hash } from 'bcrypt';
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { News,NewsLocation,NewsAuthor } from 'src/news/entities';

export default class authorSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {
    const repository = dataSource.getRepository(NewsAuthor);
    const data = {
      nombre_autor:'Jose Jimeno Jimenez'
    };

    const news = await repository.findOneBy({ nombre_autor: data.nombre_autor });

    // Insert only one record with this username.
    if (!news) {
      await repository.insert([data]);
    }

    // ---------------------------------------------------

    const authorFactory = await factoryManager.get(NewsAuthor);


    // Insert many records in database.
    await authorFactory.saveMany(5)
  }
}