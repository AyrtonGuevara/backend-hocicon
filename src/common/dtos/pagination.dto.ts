import {IsPositive, IsOptional, IsString} from 'class-validator';
import {Type} from 'class-transformer';

export class PaginationDto{
	@IsOptional()
	@IsString()
	limit?: string;

	@IsOptional()
	@IsString()
	offset?: string;
}