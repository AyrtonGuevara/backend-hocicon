import { Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import { Repository, DataSource } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { PaginationDto } from './../common/dtos/pagination.dto';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';

import { News,NewsLocation,NewsAuthor } from './entities';

@Injectable()
export class NewsService {

  constructor(
    @InjectRepository(News)
    private readonly newsRepository: Repository<News>,

    @InjectRepository(NewsLocation)
    private readonly newsLocationRepository: Repository<NewsLocation>,

    @InjectRepository(NewsAuthor)
    private readonly newsAuthorRepository: Repository<NewsAuthor>,

    private readonly dataSource: DataSource
  ){}

  async create(createNewsDto: CreateNewsDto) {
    const { lugar,autor, ...newsDetail }=createNewsDto;
    try{

      //buscando el lugar si existe en la base de datos
      let lugarExistente = await this.newsLocationRepository.findOne({
            where: { nombre_lugar: lugar },
          });
      //si no existe crear como nuevo lugar
      if(!lugarExistente){
        lugarExistente = this.newsLocationRepository.create({
          nombre_lugar: lugar,
        });
        await this.newsLocationRepository.save(lugarExistente);
      }

      //buscando el autor si existe en la base de datos
      let autorExistente = await this.newsAuthorRepository.findOne({
            where: { nombre_autor: autor },
          });
      //si no existe crear como nuevo autor
      if(!autorExistente){
        autorExistente = this.newsAuthorRepository.create({
          nombre_autor: autor,
        });
        await this.newsAuthorRepository.save(autorExistente);
      }

      const new_new = this.newsRepository.create({
        ...newsDetail,
        lugar:lugarExistente,
        autor:autorExistente,
      });

      await this.newsRepository.save( new_new );
      return {...new_new, lugar, autor};
    }catch(error){
      throw new InternalServerErrorException(error)
    }
  }

  async findAll( paginationDto:PaginationDto) {
    const{ limit=null,offset=null }=paginationDto;
    //limit=group by
    //offset=ilike
    console.log(limit); 
    const get_news = await this.newsRepository
    .createQueryBuilder('news')
    .leftJoinAndSelect('news.lugar', 'lugar')
    .leftJoinAndSelect('news.autor', 'autor');
    if(offset){
      get_news.where('news.titulo_noticia ilike :param',{param:`%${offset}%`})
      get_news.orWhere('autor.nombre_autor ilike :param',{param:`%${offset}%`})
      get_news.orWhere('lugar.nombre_lugar ilike :param',{param:`%${offset}%`})
      get_news.orWhere('news.fecha ilike :param',{param:`%${offset}%`})
    }
    if(limit){
      get_news.orderBy(`${limit}`, 'ASC');
    }
    get_news.getMany();
    const resp = await get_news.getMany();
    return resp;
  }

  async findOne(id: number) {

    const fnew = await this.newsRepository
    .createQueryBuilder('news')
    .select(['news.id', 'news.titulo_noticia', 'news.imagen_principal', 'news.fecha', 'news.contenido'])
    .where('news.id =:postid',{postid: id})
    .leftJoin('news.lugar', 'lugar')
    .addSelect('lugar.nombre_lugar')
    .leftJoin('news.autor', 'autor')
    .addSelect('autor.nombre_autor')
    .getMany();
    if(!fnew.length)
      throw new NotFoundException(`New whit id ${id} not found`);

    return fnew
  }
  
  async update(id: number, updateNewsDto: UpdateNewsDto) {
    let lugarExistente;
    let autorExistente;
    const existingNews = await this.newsRepository.findOneBy({id});

    const { lugar,autor, ...newsDetail }=updateNewsDto;

    if(!existingNews) throw new NotFoundException(`Product with id ${id} not found`)
    try{
    //si existe para modificar
      if(lugar){
        //si ya existe la actualizacion
        lugarExistente = await this.newsLocationRepository.findOne({
              where: { nombre_lugar: lugar },
            });
        //si no existe crear como nuevo lugar
        if(!lugarExistente){
        //si se desea renombrar o crear uno nuevo **
          lugarExistente = this.newsLocationRepository.create({
            nombre_lugar: lugar,
          });
          await this.newsLocationRepository.save(lugarExistente);
        }
        //actualizando
        await this.newsLocationRepository.save(lugarExistente);
      }
      //autor
      if(autor){
        //si ya existe la actualizacion
        autorExistente = await this.newsAuthorRepository.findOne({
              where: { nombre_autor: autor },
            });
        //si no existe crear como nuevo lugar
        if(!autorExistente){
        //si se desea renombrar o crear uno nuevo **
          autorExistente = this.newsAuthorRepository.create({
            nombre_autor: autor,
          });
          await this.newsAuthorRepository.save(autorExistente);
        }
        //actualizando
        await this.newsLocationRepository.save(autorExistente);
      }

      const up_new = await this.newsRepository.preload({
        id:id,
        ...existingNews,
        ...newsDetail,
        lugar:lugarExistente || existingNews.lugar,
        autor:autorExistente || existingNews.autor,
      });
      
      await this.newsRepository.save(up_new);
      return up_new;
    }catch(error){
      throw new InternalServerErrorException(error)
    }
  }

  async remove(id: number) {
    const new_delnew = await this.findOne(id);
    await this.newsRepository.remove(new_delnew);
  }
}
