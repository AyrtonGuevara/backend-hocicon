import { IsString, IsOptional, MinLength } from 'class-validator';

export class CreateNewsDto {

	@IsString()
	@MinLength(15)
	titulo_noticia:string;

	@IsString()
	@IsOptional()
	imagen_principal?:string;

	@IsString()
	lugar:string;

	@IsString()
	autor:string;

	@IsString()
	contenido:string;

}
