import {Entity, PrimaryGeneratedColumn,Column,OneToMany} from 'typeorm';
import {News} from './';

@Entity()
export class NewsAuthor{
	@PrimaryGeneratedColumn()
	id:number;

	@Column('text')
	nombre_autor:string;

	@OneToMany(
		()=>News,
		(news)=>news.autor,
		{cascade:true}
	)
	noticiasautor:News[];
}