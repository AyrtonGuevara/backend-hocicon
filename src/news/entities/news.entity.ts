import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { NewsLocation, NewsAuthor } from './';

@Entity()
export class News {

	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column('text')
	titulo_noticia:string;

	@Column('text', 
		{nullable: true })
	imagen_principal:string;

	@Column({type: 'text', default: ()=> 'CURRENT_TIMESTAMP'})
	fecha:string;

	@Column('text')
	contenido:string;

	@ManyToOne(
		()=>NewsLocation,
		(newsLocation)=>newsLocation.noticiaslugar
	)
	lugar:NewsLocation;

	@ManyToOne(
		()=>NewsAuthor,
		(newsAuthor)=>newsAuthor.noticiasautor
	)	
	autor:NewsAuthor;
} 

