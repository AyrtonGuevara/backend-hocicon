import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from 'typeorm';
import {News} from './';

@Entity()
export class NewsLocation{
	@PrimaryGeneratedColumn()
	id:number;

	@Column('text')
	nombre_lugar:string;

	@OneToMany(
		()=>News,
		(news)=>news.lugar,
		{cascade:true}
	)
	noticiaslugar:News[];
}