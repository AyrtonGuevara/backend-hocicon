export { News } from './news.entity';
export { NewsLocation } from './news-location.entity';
export { NewsAuthor } from './news-author.entity';