import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';

import { News,NewsLocation,NewsAuthor } from './entities';

@Module({
  controllers: [NewsController],
  providers: [NewsService],
  imports : [
    TypeOrmModule.forFeature([ News,NewsLocation,NewsAuthor ])
  ],
})
export class NewsModule {}
