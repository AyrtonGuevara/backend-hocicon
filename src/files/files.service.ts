import { existsSync } from 'fs';
import { Injectable,BadRequestException } from '@nestjs/common';
import { join } from 'path';


@Injectable()
export class FilesService {

	getImage(imageName:string){
		const path = join( __dirname,'../../static/uploads',imageName)
		
		if (!existsSync(path))
			throw new BadRequestException(`image not found ${imageName}`);
		return path;
	}
}
