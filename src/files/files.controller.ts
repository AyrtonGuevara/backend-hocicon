import { Controller, Get, Post, Body, Patch, Param, Delete, UploadedFile,UseInterceptors,BadRequestException,Res } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { FilesService } from './files.service';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';

import { fileFilter,fileNamer } from './helpers';


@Controller('files')
export class FilesController {
  constructor(
    private readonly filesService: FilesService,
    private readonly configService: ConfigService,
  ) {}

  @Get('image/:imageName')
  findOneImage(
    @Res() res:Response,
    @Param('imageName') imageName:string
    ){
    const path = this.filesService.getImage(imageName);
    res.sendFile(path)
  }

  @Post('image')
  @UseInterceptors(FileInterceptor('file',{
    fileFilter:fileFilter,
    storage: diskStorage({
      destination: './static/uploads',
      filename: fileNamer
    })
  }))
  uploadImage(
    @UploadedFile() file:Express.Multer.File
  ){
    if(!file) throw new BadRequestException('Make sure that the file is an image');
    const imageUrl=`${this.configService.get('HOST_API')}/files/image/${file.filename}`
    return file.filename;
  }
}
