FROM node:20.10 as deps
WORKDIR /app

COPY package.json tsconfig.json ./
RUN npm install --frozen-lockfile


FROM node:20.10 as builder
WORKDIR /app

COPY --from=deps /app/node_modules ./node_modules

COPY . .
RUN npm run build
	

FROM node:20.10 as runner
WORKDIR /app

COPY package.json tsconfig.json ./
RUN npm install --prod
COPY --from=builder /app/dist/src ./dist/src

CMD [ "node","dist/src/main" ]